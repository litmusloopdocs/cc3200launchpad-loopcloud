# CC3200-loopmq

The **CC3200 LaunchPad** development kit is an evaluation development platform for the CC3200 wireless microcontroller (MCU), the industry�s first single-chip programmable MCU with built-in Wi-Fi connectivity.

## Overview

This project is to demonstrate the implementation of MQTT.**Message Queue Telemetry Transport (MQTT)** is an extremely simple and lightweight messaging protocol, designed for constrained devices and low bandwidth, high latency and unreliable networks. The protocol uses **publish/subscribe** communication pattern and is used for machine to machine communication and plays and important role in the internet of things. MQTT works on the **TCP/IP connection**.

The library provides an example of publish and subscribe messaging with a server that supports MQTT using **CC3200- launchpadXL**.

***Features provided by the client library:***

* Connect the device to any IP network using Ethernet, Wi-Fi, 4G/LTE
* Publish any message to the MQTT server in standard JSON format on a specific topic 
* Subscribe data from the server to the device on a specific topic
* Unsubscribe the topic to no longer communicate to the device
* Disconnect the device from any network connectivity.

***The following Table shows status of the client and the server when the above functions are implemented:***

> |Function |  Server Status   |    Client Status
> ----------------|------------------|---------------
|Looopmq.connect | Connected       | Connected|
|Loopmq.publish |Connected |  Connected|
|Loopmq.subscribe | Connected | Connected|
|Loopmq.unsubscribe | Connected | Disconnected|
|Loopmq.disconnect |  Disconnected |  Disconnected|


## Getting Started with CC3200

Basic steps to connect CC3200 to internet are given below:

1. Connect the CC3200 to the laptop with the USB cable 
2. Download the Energia software from [here](http://energia.nu/download/)
3. Open device manager to check the port of the device and select the same in Energia, download and install the [driver](http://energia.nu/guide/guide_windows/) if COM port not recognized
4. Select the LauncPad w/cc3200 (80 MHz) from the board and you are ready to load the program.
5. The SOP-2 Jumper setting should be as shown below when you want to upload a code to the device

    ![alt text](https://bytebucket.org/litmusloopdocs/cc3200launchpad-loopcloud/raw/master/extras/Jumper_to_upload_code.png)

6.  Remove he SOP-2 jumper as shown below and press the reset to execute the code

    ![alt text](https://bytebucket.org/litmusloopdocs/cc3200launchpad-loopcloud/raw/master/extras/Jumper_to_execute_code.png)

## Configuration

The user need to define a list of parameters in order to connect the device to a server in a secured manner.

Below are the list of minimum definitions required by the user to send data to the cloud.

```
#define ssid "LitmusAutomation-Internal"             // network ssid
#define network_password "Zxcvbn<>890"         	     // network password
#define port_number 1883                             // Port number
#define server "loopdocker1.cloudapp.net"            // Server name
#define clientID "client_CC3200"                     // ClientID
#define password "password"                          // password
#define userID "admin"                               // username 
#define subTOPIC "cc3200/loop1"                      // Subscribe on this topic to get the data
#define pubTopic "cc3200/loop2"                      // Publish on this tpoic to send data or command to device 
```

## Functions

1.***loopmq.connect (client ID)***

This function is used to connect the device or the client to the client ID specified by the user.

```
if (loopmq.connect(c)) {
      Serial.println ("connected");
```       

2.***loopmq.connect (client ID, username, password)***

Checks for the username and password specified by the user to connect the device to the network.

```
 if (loopmq.connect(c, user, pass)){
   counter();      // Publish 1-100 counter to the server
```

3.***loopmq.publish (topic, data)***

This function is used to publish data in string format to the topic specified by the user. 
In this example a counter from *1 to 100* is passed to the topic *litmus* specified by the user.

```
loopmq.publish (p,var);        // Publish message to the server
```

4.***loopmq.subscribe (topic)***

This function is used to subscribe to a topic to which data will be published from the user to the		 device. 

```
loopmq.subscribe(s);                    // Subscribe to a topic
```

5.***loopmq.unsubscribe (topic)***

This function is used to unsubscribe the device from the server. Calling this function will stop sending messages from the device to the server.

```
// loopmq.unsubscribe(s);   Note: uncomment the code to unsubscribe from the topic
```
 
6.***lopmq.disconnect ()***

This function is used to disconnect the device from the server. Disconnect does not stop the functionality of the device but disconnects it from the network. The device works fine locally but does not send any update to the internet.

```
// loopmq.disconnect();  Note: uncomment the code to disconnect the device
```

7.***JSON PARSER***

This function is used to create a JSON payload to be passed to the broker as payload. Please refer the [link](https://github.com/bblanchon/ArduinoJson/wiki/Compatibility-issues) for any compalibility issues.

```
  StaticJsonBuffer<200> jsonBuffer;               //Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase that value. 

  JsonObject& root = jsonBuffer.createObject();   //It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "sensor";                     //Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;
  root["number"]= var;

  JsonObject& data= root.createNestedObject("data"); //nested JSON 
  data["number"]=var;                                //Add data["key"]= value
  data["Litmus"]="Loop";


  root.printTo(Serial);                           //prints to serial terminal
  Serial.println();

  char buffer[100];                               //buffer to pass as payload
  root.printTo(buffer, sizeof(buffer));           //copy the JSON to the buffer to pass as a payload 

```