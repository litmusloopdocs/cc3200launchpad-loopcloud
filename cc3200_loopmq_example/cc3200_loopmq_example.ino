
/*
 Litmus Loop
*/

/*
Inlcude all the library files 
*/
#include <loopmq.h>
#include <WiFi.h>
#include <SPI.h>
#include <stdlib.h>
#include <stdio.h>
#include <Wire.h>
#include <ArduinoJson.h>
#include "configuration.h"

/*
Passing the defined information to char
*/

int port = port_number;						          // port number
char ssd[] = ssid;							            // network ssid
char passd[] = network_password;			      // network password
char hostname[] = server;                   // Server name
char c[15] = clientID;                      // ClientID
char pass[16]= password;                    // password
char user[16] = userID;                     // username
char p[]= subTOPIC;                         // Subscribe on this topic to get the data
char s[]= pubTopic;                         // Publish on this topic to send data or command to device 

char var[16];                               // data published on the topic

int num=0;                                  // intermediate variable for the counter function
String ledstatus;                           // Status of the lED

/*
Function to generate a counter to be published on a particular topic
*/
char *counter(){
    while(1){
    if(num < 100 ){    
      num++;
      sprintf(var,"%d",num);
   return var;
    }
    else if (num = 100)
      num = 0;
    } 
}


/*
Function to display the message that are given by the user to the device. 
*/

void callback(char* topic, byte* payload, unsigned int length) {
 Serial.print("Message arrived [");
 Serial.print(topic);
 Serial.print("] ");

/*
Display the message published serially and on the LCD
*/
  for (int i=0;i<length;i++) {
    Serial.print((char)payload[i]);           // Serial Display
//    lcd.print((char)payload[i]);            // LCD Display
  }
     Serial.println();                        // Print on a new line
}


WiFiClient client_CC3200;                     // yun client
PubSubClient loopmq(client_CC3200);           // instance to use the loopmq functions.

void reconnect() {                        
   
  while (!loopmq.connected()) {                     // Loop until we're reconnected
  Serial.print("Attempting MQTT connection...");    // Attempt to connect    
    if (loopmq.connect(c,user,pass)) {
    Serial.println("connected");                    // Once connected, publish an announcement...

        loopmq.subscribe(s);                        // Subscribe to a topic  
/*  
To Unscubscribe from a Topic uncomment the code below   
*/
        
   // loopmq.unsubscribe(s); 
   
/*
To disconnect unocmment the code below
*/
      
   //  loopmq.disconnect();            
    } 
    else {
   Serial.print("failed, rc=");
   Serial.print(loopmq.state());
   Serial.println(" try again in 5 seconds");
      
      delay(5000);                                  // Wait 5 seconds before retrying
    }
  }
}

void setup()
{
  WiFi.begin(ssd, passd);
  
  pinMode(RED_LED,OUTPUT);                            // Define the output LED pin 
  pinMode(GREEN_LED,OUTPUT);                          // Define the output LED pin
  
  Serial.begin(57600);
    
  loopmq.setServer(hostname, port);                   // Connect to the specified server and port defined by the user
  loopmq.setCallback(callback);                       // Call the callbeck funciton when published     

  delay(1500);                                        // Allow the hardware to sort itself out

}

void loop()
{

  if((num%2)==0){
    ledstatus = "RED" ;
    digitalWrite(GREEN_LED,LOW);
    digitalWrite(RED_LED,HIGH);   
    Serial.print(var);
    }
  else{
    ledstatus = "GREEN" ;
    digitalWrite(RED_LED,LOW);
    digitalWrite(GREEN_LED,HIGH);
    Serial.print(var);  
  }

  
/*
JSON parser
*/
  StaticJsonBuffer<200> jsonBuffer;                 // Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase that value. 

  JsonObject& root = jsonBuffer.createObject();     // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "CC3200 Launchpad_XL";          // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;

  JsonObject& data= root.createNestedObject("data"); // nested JSON 
  data["number"]=var;                                // Add data["key"]= value
  data["LedStatus"]=ledstatus;


  root.printTo(Serial);                             // prints to serial terminal
  Serial.println();

  char buffer[100];                                 // buffer to pass as payload
  root.printTo(buffer, sizeof(buffer));             // copy the JSON to the buffer to pass as a payload 

/*
Publish to the server
*/
 if (!loopmq.connected()) {
    reconnect();                                    // Try to reconnect if connection dropped
  }
 if (loopmq.connect(c, user, pass)){
   counter();
 
   loopmq.publish(p,buffer);                        // Publish message to the server once only
     delay(1000);
  }
  
 loopmq.loop();                                     // check if the network is connected and also if the client is up and running
}

